from model.model import Model
from view.userInterface import UserInterface


class Controller(UserInterface):
    # Constants #
    STRING_ZERO = '0'
    STRING_ONE = '1'
    STRING_EMPTY = ''
    STRING_SPACE = ' '
    NUMERIC_MINUS_ONE = -1
    NUMERIC_ZERO = 0
    NUMERIC_ONE = 1

    def __init__(self):
        super().__init__()
        self.args = []
        self.argTmp = self.STRING_EMPTY
        self.history = {}
        self.ui_instance = UserInterface()
        self.ui_instance.initGui()
        self.bindButtons(self.ui_instance.buttons)

    def on_press(self, button):
        self.updateGui(button)

    def bindButtons(self, buttons):
        """
        This function binds buttons from a list of buttons to an event and an action
        :param buttons: List<Button>
        :return:
        """
        for button in buttons:
            button.bind(on_press=self.on_press)
        return buttons

    @staticmethod
    def formatFormula(arg):
        """
        This function formats computing vars by adding spaces around operators for better readability
        :param arg: Array
        :return: formatted string
        """
        result = ''
        for char in arg:
            if not char.isdigit() and char != Model.DOT_KEY:
                result += f" {char} "
            else:
                result += char
        return result

    def updateGui(self, button):
        """
        This function updates the GUI depending on the button clicked by the user given in parameter
        :param button: Button
        :return: None
        """
        stringBtnValue = button.text
        numericBtnValue: int = self.NUMERIC_MINUS_ONE
        result = self.STRING_EMPTY
        try:
            # If numeric value of the button
            numericBtnValue = int(stringBtnValue)
            self.handleNumerics(stringBtnValue)
        except ValueError:
            # If not numeric value of the button
            # If button clicked = '.' (dot)
            if stringBtnValue == Model.DOT_KEY:
                self.handleDotKey(stringBtnValue)
            # If button clicked = backspace (del)
            elif stringBtnValue == Model.BACK_KEY:
                self.handleBackKey(stringBtnValue)
            # If button clicked = equals ('=')
            elif stringBtnValue == Model.EQUALS_KEY:
                self.handleEqualsKey(stringBtnValue)
            # If button clicked = Clear ('C')
            elif stringBtnValue == Model.CLEAR_KEY:
                # sets all fields to empty and resultLabel to '0'
                self.resetAllFields(self, True, False)
            # If button clicked is an operator ['/', '*', '-', '+']
            elif stringBtnValue in Model.LIST_OPERATOR_KEYS:
                if len(self.args) != self.NUMERIC_ZERO:
                    self.handleOperatorKeys(stringBtnValue)

    def doCompute(self, stringBtnValue):
        """
        This function computes three arguments contained in an array (two values and an operator)
        Keeps the result and the value of the last button clicked in an array
        :param stringBtnValue: String
        :return: None
        """
        result = self.NUMERIC_MINUS_ONE
        firstOperand = self.STRING_EMPTY
        secondOperand = self.STRING_EMPTY
        sign = self.STRING_EMPTY
        index: int = self.NUMERIC_ZERO
        # Splits the array into three different string vars
        for item in self.args:
            if item in Model.LIST_OPERATOR_KEYS:
                firstOperand = ''.join(map(str, self.args[:index]))
                sign = str(self.args[index])
                secondOperand = ''.join(map(str, self.args[index + self.NUMERIC_ONE:]))
            index += self.NUMERIC_ONE
        # Calculation
        if sign == '/':
            result = round(float(float(firstOperand) / float(secondOperand)), 2)
        elif sign == "*":
            result = round(float(float(firstOperand) * float(secondOperand)), 2)
        elif sign == '-':
            result = round(float(float(firstOperand) - float(secondOperand)), 2)
        elif sign == '+':
            result = round(float(float(firstOperand) + float(secondOperand)), 2)
        result = str(result)
        # If result ends with '.0', suppressing it
        if result.endswith('.0'):
            result = result[:-2]
        self.args.clear()
        # Keeping result and last operator (if so) in memory for next computing
        self.args.append(result)
        if stringBtnValue in Model.LIST_OPERATOR_KEYS:
            self.args.append(stringBtnValue)
        # Showing formula input
        self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
        # Showing result
        self.ui_instance.resultLabel.text = result
        return result

    @staticmethod
    def isEndsOperator(self, tab):
        """
        This function checks if the last char in the array given in parameter is an operator (eg : ['/', '*', '-', '+'])
        :param self: self
        :param tab: Array
        :return: Boolean
        """
        isContainsOperator = False
        if len(tab) != self.NUMERIC_ZERO:
            if tab[len(tab) - self.NUMERIC_ONE] in Model.LIST_OPERATOR_KEYS:
                isContainsOperator = True
        return isContainsOperator

    @staticmethod
    def isContainsOperator(tab):
        """
        This function checks if there is an operator in the array given in parameter (eg : ['/', '*', '-', '+'])
        :param tab: Array
        :return: Boolean
        """
        isContainsOperator = False
        for item in tab:
            if item in Model.LIST_OPERATOR_KEYS:
                isContainsOperator = True
        return isContainsOperator

    @staticmethod
    def isEndsDot(self, tab):
        """
        This function checks if the last char in the array given in parameter is a dot
        :param self: self
        :param tab: Array
        :return: Boolean
        """
        isEndsDot = False
        if len(tab) != self.NUMERIC_ZERO:
            if tab[len(tab) - self.NUMERIC_ONE] == Model.DOT_KEY:
                isEndsDot = True
        return isEndsDot

    @staticmethod
    def isContainsDot(self, tab):
        """
        This function checks if there is a dot in the array given in parameter
        :param self: self
        :param tab: Array
        :return: Boolean
        """
        isContainsDot = False
        if len(tab) != self.NUMERIC_ZERO:
            for item in tab:
                if item in Model.DOT_KEY:
                    isContainsDot = True
        return isContainsDot

    @staticmethod
    def isEndsEquals(self, tab):
        """
        This function checks if the last char in the array given in parameter is an equals sign '='
        :param self: self
        :param tab: Array
        :return: Boolean
        """
        isEndsEquals = False
        if len(tab) != self.NUMERIC_ZERO:
            if tab[len(tab) - self.NUMERIC_ONE] == Model.EQUALS_KEY:
                isEndsEquals = True
        return isEndsEquals

    @staticmethod
    def resetAllFields(self, resetResultLabel: bool, removeOnlyLastChar: bool):
        """
        This function resets fields to empty or 0, or remove only the last char depending on given parameters
        :param self: self
        :param resetResultLabel, removeOnlyLastChar: bool
        :return: None
        """
        # Resets all fields to empty except resultLabel which is set to '0'
        if not removeOnlyLastChar:
            self.ui_instance.formulaLabel.text = self.STRING_EMPTY
            if resetResultLabel:
                self.ui_instance.resultLabel.text = self.STRING_ZERO
            self.argTmp = self.STRING_EMPTY
            self.args.clear()
        # Replaces only the last char in each field
        else:
            if self.ui_instance.formulaLabel.text != self.STRING_EMPTY:
                # If the last char is not an operator, removes all beyond the operator, if there is one
                if self.argTmp[self.NUMERIC_MINUS_ONE] not in Model.LIST_OPERATOR_KEYS:
                    index = self.NUMERIC_ZERO
                    for item in self.argTmp:
                        if item in Model.LIST_OPERATOR_KEYS:
                            self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp[:index
                                                                                                 + self.NUMERIC_ONE])
                            break
                        index += self.NUMERIC_ONE
                # Removes only the last char instead
                else:
                    self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp[:self.NUMERIC_MINUS_ONE])
            if resetResultLabel:
                # If resultLabel contains only one char setting it to '0'
                if len(self.ui_instance.resultLabel.text) == self.NUMERIC_ONE:
                    self.ui_instance.resultLabel.text = self.STRING_ZERO
                # Removing the last char instead
                else:
                    self.ui_instance.resultLabel.text = self.ui_instance.resultLabel.text[:self.NUMERIC_MINUS_ONE]
            # Removing the last char in computing vars
            self.argTmp = self.argTmp[:self.NUMERIC_MINUS_ONE]
            del (self.args[len(self.args) - self.NUMERIC_ONE])

    def handleNumerics(self, stringBtnValue):
        """
        This function appends or replaces fields values depending on the last char of the computing vars
        :param stringBtnValue: string
        :return: None
        """
        if self.isEndsEquals(self, self.argTmp):
            self.resetAllFields(self, True, False)
            self.argTmp = stringBtnValue
            self.ui_instance.resultLabel.text = stringBtnValue
        elif self.isEndsOperator(self, self.args):
            self.argTmp += stringBtnValue
            self.ui_instance.resultLabel.text = stringBtnValue
        elif self.ui_instance.resultLabel.text != self.STRING_ZERO or self.isEndsDot(self, self.argTmp):
            self.argTmp += stringBtnValue
            self.ui_instance.resultLabel.text += stringBtnValue
        else:
            self.argTmp = stringBtnValue
            self.ui_instance.resultLabel.text = self.formatFormula(self.argTmp)
        self.args.append(stringBtnValue)

    def handleDotKey(self, stringBtnValue):
        """
        This function appends or replaces fields values depending on the last char of the computing vars
        :param stringBtnValue: string
        :return: None
        """
        # If not computing vars ends with equals sign (meaning computing already done ==> can't append anything
        if not self.isEndsEquals(self, self.argTmp):
            # Appends dot if resultLabel is not equals 0
            if self.ui_instance.resultLabel.text != self.STRING_ZERO:
                if not self.isEndsDot(self, self.args):
                    self.argTmp += stringBtnValue
                    self.args.append(stringBtnValue)
                    self.ui_instance.resultLabel.text += stringBtnValue
            else:
                # Appends 0 + dot if clicked dot button without entering anything before
                if len(self.args) == self.NUMERIC_ZERO:
                    self.argTmp += self.ui_instance.resultLabel.text + stringBtnValue
                    self.args.append(self.ui_instance.resultLabel.text + stringBtnValue)
                # Appends dot in other cases
                else:
                    self.argTmp += stringBtnValue
                    self.args.append(stringBtnValue)
                self.ui_instance.resultLabel.text += stringBtnValue

    def handleBackKey(self, stringBtnValue):
        """
        This function replaces fields values depending on the last char of the computing vars
        :param stringBtnValue: string
        :return: None
        """
        if len(self.ui_instance.resultLabel.text) > self.NUMERIC_ONE:
            # If last char in computing vars is an operator, removing it and leaving resultLabel as it is
            if self.argTmp[self.NUMERIC_MINUS_ONE] in Model.LIST_OPERATOR_KEYS:
                self.resetAllFields(self, False, True)
            # If computing vars contains only one char, sets all fields to empty and resultLabel to '0'
            elif len(self.args) == self.NUMERIC_ONE:
                self.resetAllFields(self, True, False)
            # Removing only last char in all fields in other cases
            else:
                self.resetAllFields(self, True, True)
        elif len(self.ui_instance.resultLabel.text) == self.NUMERIC_ONE:
            if self.ui_instance.resultLabel.text != self.STRING_ZERO:
                # If computing vars contains only sign equals, sets all fields to empty and resultLabel to '0'
                if self.argTmp[self.NUMERIC_MINUS_ONE] == Model.EQUALS_KEY:
                    self.resetAllFields(self, True, False)
                # Removing last char in all fields instead
                else:
                    self.resetAllFields(self, True, True)

    def handleEqualsKey(self, stringBtnValue):
        """
        This function replaces fields values depending on the last char of the computing vars
        :param stringBtnValue: string
        :return: None
        """
        # If computing vars contains an operator
        if self.isContainsOperator(self.args):
            # If last char of computing vars is digit, computes and appends value to appropriate fields
            if self.argTmp[self.NUMERIC_MINUS_ONE] in Model.LIST_NUMERIC_KEYS:
                self.argTmp += stringBtnValue
                self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
                self.ui_instance.resultLabel.text = self.doCompute(stringBtnValue)
                self.history[self.ui_instance.resultLabel.text] = self.ui_instance.formulaLabel.text
            # If last char of computing vars is an operator (eg : user typed '2' then '+' then '=')
            # ==> computes (first operand : operator : first operand) (eg : 2 + 2)
            elif self.argTmp[self.NUMERIC_MINUS_ONE] in Model.LIST_OPERATOR_KEYS:
                self.args.clear()
                self.args.append(self.ui_instance.resultLabel.text)
                self.args.append(self.argTmp[self.NUMERIC_MINUS_ONE])
                self.args.append(self.argTmp[:self.NUMERIC_MINUS_ONE])
                self.argTmp = (self.ui_instance.resultLabel.text + self.argTmp[self.NUMERIC_MINUS_ONE]
                               + self.argTmp[:self.NUMERIC_MINUS_ONE] + stringBtnValue)
                self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
                self.ui_instance.resultLabel.text = self.doCompute(stringBtnValue)
                # Saving history not used anywhere !!!
                self.history[self.ui_instance.resultLabel.text] = self.ui_instance.formulaLabel.text

    def handleOperatorKeys(self, stringBtnValue):
        """
        This function replaces fields values depending on the last char of the computing vars
        or computes if computing vars are adequate
        :param stringBtnValue: string
        :return: None
        """
        try:
            # If last char of computing vars is digit, appends value to computing vars
            numericBtnValue = int(self.args[len(self.args) - self.NUMERIC_ONE])
            self.argTmp += stringBtnValue
            self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
            if not self.isEndsOperator(self, self.args):
                self.args.append(stringBtnValue)
            for item in self.args:
                # If last char of computing vars is digit
                if self.args[len(self.args) - self.NUMERIC_ONE] in Model.LIST_NUMERIC_KEYS:
                    # If computing vars contains operator then computing (eg 2+3+ ==> 5+)
                    if item in Model.LIST_OPERATOR_KEYS:
                        result = self.doCompute(stringBtnValue)
                        self.argTmp = result + stringBtnValue
                        self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
        except ValueError:
            # If last char of computing vars is not digit, replaces last char by new value
            if not self.isEndsEquals(self, self.argTmp):
                self.argTmp = self.argTmp[:self.NUMERIC_MINUS_ONE] + stringBtnValue
                self.ui_instance.formulaLabel.text = self.formatFormula(self.argTmp)
                self.args[len(self.args) - self.NUMERIC_ONE] = stringBtnValue


