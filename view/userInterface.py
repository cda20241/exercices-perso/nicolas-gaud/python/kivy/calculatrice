from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label

from model.model import Model

Window.size = (400, 400)


class UserInterface:
    def __init__(self):
        self.rootLayout = None
        self.formulaLabel = None
        self.resultLabel = None
        self.resLayout = None
        self.gridKeysLayout = None
        self.buttons = []
        self.initGui()

    def initGui(self):
        self.rootLayout = BoxLayout(orientation='vertical')

        self.resLayout = BoxLayout(orientation='vertical')
        self.resLayout.rows = 2

        self.gridKeysLayout = GridLayout(cols=4, rows=5, padding=[5, 5, 5, 5], spacing=5)

        self.formulaLabel = Label(text='')
        self.resLayout.add_widget(self.formulaLabel)

        self.resultLabel = Label(text='0')
        self.resLayout.add_widget(self.resultLabel)

        for item in Model.LIST_BUTTONS:
            self.buttons.append(self.addButton(item))

        self.rootLayout.add_widget(self.resLayout)
        self.rootLayout.add_widget(self.gridKeysLayout)

        return self.rootLayout

    def addButton(self, text):
        button = Button(text=text)
        self.gridKeysLayout.add_widget(button)
        self.buttons.append(button)

        return button
