class Model:
    LIST_NUMERIC_KEYS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    LIST_OPERATOR_KEYS = ['/', '*', '-', '+']
    CLEAR_KEY = 'Clr'
    DOT_KEY = '.'
    EQUALS_KEY = '='
    SQUARE_KEY = '^2'
    PERCENT_KEY = '%'
    BACK_KEY = "Bck"
    LIST_BUTTONS = [SQUARE_KEY,
                    CLEAR_KEY,
                    BACK_KEY,
                    LIST_OPERATOR_KEYS[0],
                    LIST_NUMERIC_KEYS[7],
                    LIST_NUMERIC_KEYS[8],
                    LIST_NUMERIC_KEYS[9],
                    LIST_OPERATOR_KEYS[1],
                    LIST_NUMERIC_KEYS[4],
                    LIST_NUMERIC_KEYS[5],
                    LIST_NUMERIC_KEYS[6],
                    LIST_OPERATOR_KEYS[2],
                    LIST_NUMERIC_KEYS[1],
                    LIST_NUMERIC_KEYS[2],
                    LIST_NUMERIC_KEYS[3],
                    LIST_OPERATOR_KEYS[3],
                    PERCENT_KEY,
                    LIST_NUMERIC_KEYS[0],
                    DOT_KEY,
                    EQUALS_KEY
                    ]
